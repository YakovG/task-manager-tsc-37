CREATE TABLE `tm_task` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`projectId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`created` BIGINT(20) NULL DEFAULT NULL,
	`dateStart` BIGINT(20) NULL DEFAULT NULL,
	`dateFinish` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
