CREATE TABLE `tm_project` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`created` BIGINT(20) NOT NULL DEFAULT '0',
	`dateStart` BIGINT(20) NOT NULL DEFAULT '0',
	`dateFinish` BIGINT(20) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
;
