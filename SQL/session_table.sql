CREATE TABLE `tm_session` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`timestamp` BIGINT(20) NOT NULL DEFAULT '0',
	`userId` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`signature` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
