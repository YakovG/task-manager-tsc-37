package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.ProjectRepository;
import ru.goloshchapov.tm.repository.UserRepository;

import java.sql.Connection;

import static ru.goloshchapov.tm.util.HashUtil.salt;


public final class ProjectServiceTest implements ServiceLocator{

    private Session session;

    private ServiceLocator serviceLocator;

    private User user;

    private ConnectionService connectionService;

    private UserService userService;

    private PropertyService propertyService;

    private Project project;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    public @NotNull IConnectionService getConnectionService() {
        return connectionService;
    }

    @Before
    public void before() {
        serviceLocator = this;
        session = new Session();
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        user = new User();
        user.setLogin("test");
        final String password = "test";
        user.setPasswordHash(salt(propertyService, password));
        userService = new UserService(serviceLocator);
        userService.add(user);
        project = new Project();
        project.setUserId(user.getId());
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        final ProjectRepository projectRepository = new ProjectRepository(connection);
        projectRepository.clear();
        connection.commit();
        final UserRepository userRepository = new UserRepository(connection);
        userRepository.clear();
        connection.commit();
        connection.close();
    }

    @Test
    @Category(DataCategory.class)
    public void testStartFinish() {
        final ProjectService projectService = new ProjectService(serviceLocator);
        Assert.assertNotNull(projectService);
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        project.setUserId(user.getId());
        Assert.assertEquals(user.getId(), project.getUserId());
        Assert.assertNotNull(projectService.add(project));
        Assert.assertFalse(projectService.findAll().isEmpty());
        @Nullable Project project1 = projectService.startOneById(user.getId(), project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(),project1.getId());
        Assert.assertEquals(Status.IN_PROGRESS,project1.getStatus());
        project1 = projectService.finishOneById(user.getId(), project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(),project1.getId());
        Assert.assertEquals(Status.COMPLETE,project1.getStatus());
    }

    @Test
    @Category(DataCategory.class)
    public void testAddUpdate() {
        final ProjectService projectService = new ProjectService(serviceLocator);
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertNotNull(projectService.add(user.getId(),"PROJECT","DESCRIPTION"));
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(projectService.findAllByUserId(user.getId()).isEmpty());
        Assert.assertEquals("DESCRIPTION", projectService.findOneByName("PROJECT").getDescription());
        Assert.assertNotNull(projectService.findOneByName("PROJECT"));
        final Project project1 = projectService.findOneByName("PROJECT");
        Assert.assertNotNull(
                projectService.updateOneById(user.getId(), project1.getId(), "NAME", "DESC")
        );
        Assert.assertEquals("NAME", projectService.findOneById(project1.getId()).getName());
        Assert.assertEquals("DESC", projectService.findOneById(project1.getId()).getDescription());
    }

    @Test
    @Category(DataCategory.class)
    public void testChangeStatus() {
        final ProjectService projectService = new ProjectService(serviceLocator);
        Assert.assertTrue(projectService.findAll().isEmpty());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        project.setName("PROJECT");
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("PROJECT", project.getName());
        project.setUserId(user.getId());
        Assert.assertEquals(user.getId(), project.getUserId());
        Assert.assertNotNull(projectService.add(project));
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertNotNull(
                projectService.changeOneStatusByName(user.getId(),"PROJECT", "COMPLETE")
        );
        Assert.assertEquals(Status.COMPLETE, projectService.findOneByName("PROJECT").getStatus());
        Assert.assertNotNull(
                projectService.changeOneStatusById(user.getId(),project.getId(), "IN_PROGRESS")
        );
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(project.getId()).getStatus());
    }
}

