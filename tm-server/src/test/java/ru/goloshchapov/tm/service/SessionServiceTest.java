package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.repository.SessionRepository;
import ru.goloshchapov.tm.repository.UserRepository;


import java.sql.Connection;

import static ru.goloshchapov.tm.util.HashUtil.salt;

public final class SessionServiceTest implements ServiceLocator{

    private Session session;

    private ServiceLocator serviceLocator;

    private User user;

    private ConnectionService connectionService;

    private UserService userService;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return connectionService;
    }

    @Before
    public void before() {
        serviceLocator = this;
        session = new Session();
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        user = new User();
        user.setLogin("test");
        final String password = "test";
        user.setPasswordHash(salt(propertyService, password));
        userService = new UserService(serviceLocator);
        userService.add(user);
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getConnectionService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        sessionRepository.clear();
        connection.commit();
        final UserRepository userRepository = new UserRepository(connection);
        userRepository.clear();
        connection.commit();
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testOpenClose() {
        SessionService sessionService = new SessionService(serviceLocator);
        Assert.assertNotNull(sessionService);
        Assert.assertNotNull(sessionService.open("test","test"));
        Assert.assertNotNull(sessionService.findSessionByLogin("test"));
        session = sessionService.findSessionByLogin("test");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(user.getId(),session.getUserId());
        sessionService.close(session);
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCloseByLogin() {
        SessionService sessionService = new SessionService(serviceLocator);
        Assert.assertNotNull(sessionService);
        Assert.assertNotNull(sessionService.open("test","test"));
        Assert.assertNotNull(sessionService.findSessionByLogin("test"));
        session = sessionService.findSessionByLogin("test");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(user.getId(),session.getUserId());
        sessionService.closeSessionByLogin("test","test");
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRootSession() {
        SessionService sessionService = new SessionService(serviceLocator);
        Assert.assertNotNull(sessionService);
        Assert.assertNotNull(sessionService.open("root","root"));
        Assert.assertNotNull(sessionService.findSessionByLogin("root"));
        session = sessionService.findSessionByLogin("root");
        Assert.assertTrue(sessionService.isValid(session));
        Assert.assertEquals(userService.getRootUser().getId(),session.getUserId());
        sessionService.closeSessionByLogin("root","root");
    }
}
