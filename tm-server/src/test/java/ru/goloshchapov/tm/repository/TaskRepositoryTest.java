package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

import java.sql.Connection;

public final class TaskRepositoryTest implements ServiceLocator {

    private User user;

    private Project project;

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull ITestService getTestService() {
        return testService;
    }

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestTaskTable();
        user = new User();
        user.setId("ttt");
        project = new Project();
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        taskRepository.clear();
        connection.commit();
        connection.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testCreate() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        final Task taskById = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemove() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        final Task taskById = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        Assert.assertNotNull(taskRepository.removeOneById(taskById.getId()));
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testClear() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        final Task task1 = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertNotNull(taskRepository.add(task1));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        taskRepository.clear();
        connection.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testSize() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        final Task task1 = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertNotNull(taskRepository.add(task));
        Assert.assertNotNull(taskRepository.add(task1));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(2,taskRepository.size());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFindAllByProjectId() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("pppttt");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("pppttt", task.getProjectId());
        task.setUserId(user.getId());
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "pppttt").isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testRemoveAllByProjectId() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setProjectId("PROJECT_ID");
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals("PROJECT_ID", task.getProjectId());
        task.setUserId(user.getId());
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        Assert.assertFalse(taskRepository.removeAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        connection.commit();
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), "PROJECT_ID").isEmpty());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testBindUnbind() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final TaskRepository taskRepository = new TaskRepository(connection);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        final Task task = new Task();
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        task.setName("TASK");
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("TASK", task.getName());
        task.setUserId(user.getId());
        Assert.assertNotNull(task.getUserId());
        Assert.assertEquals(user.getId(), task.getUserId());
        Assert.assertNotNull(taskRepository.add(task));
        connection.commit();
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        Assert.assertNotNull(taskRepository.bindToProjectById(user.getId(), task.getId(), project.getId()));
        connection.commit();
        Assert.assertFalse(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        Assert.assertNotNull(taskRepository.unbindFromProjectById(user.getId(), task.getId()));
        connection.commit();
        Assert.assertTrue(taskRepository.findAllByProjectId(user.getId(), project.getId()).isEmpty());
        connection.close();
    }
}
