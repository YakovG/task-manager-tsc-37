package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.marker.DataCategory;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.service.PropertyService;
import ru.goloshchapov.tm.service.TestService;

import java.sql.Connection;
import java.util.Date;

public final class SessionRepositoryTest implements ServiceLocator{

    private ServiceLocator serviceLocator;

    private PropertyService propertyService;

    private TestService testService;

    @Override
    public @NotNull IConnectionService getConnectionService() {
        return null;
    }

    @Override
    public @NotNull ITestService getTestService() { return testService;}

    @Override
    public @NotNull ITaskService getTaskService() {
        return null;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return null;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return null;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return null;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return null;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return null;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return null;
    }

    @Before
    @SneakyThrows
    public void before() {
        serviceLocator = this;
        propertyService = new PropertyService();
        testService = new TestService(propertyService);
        testService.initTestSessionTable();
    }

    @After
    @SneakyThrows
    public void after() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        sessionRepository.clear();
        connection.commit();
        connection.close();
        testService.dropDatabase();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testMerge() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        connection.commit();
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testFindByUserId() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        connection.commit();
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        Assert.assertNotNull(sessionRepository.findByUserId("test"));
        final Session session1 = sessionRepository.findByUserId("test");
        Assert.assertNotNull(session1);
        Assert.assertEquals(sessionById.getId(), session1.getId());
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testContains() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(session.getId());
        final String sessionId = session.getId();
        Assert.assertFalse(sessionRepository.contains(sessionId));
        Assert.assertNotNull(sessionRepository.merge(session));
        connection.commit();
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertTrue(sessionRepository.contains(sessionId));
        connection.close();
    }

    @Test
    @SneakyThrows
    @Category(DataCategory.class)
    public void testExclude() {
        final Connection connection = serviceLocator.getTestService().getConnection();
        final SessionRepository sessionRepository = new SessionRepository(connection);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        final Session session = new Session();
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        session.setUserId("test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals("test", session.getUserId());
        @NotNull final Date date = new Date();
        session.setTimestamp(date.getTime());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertEquals(date, new Date(session.getTimestamp()));
        session.setSignature("sign");
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("sign", session.getSignature());
        Assert.assertNotNull(sessionRepository.merge(session));
        connection.commit();
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        final Session sessionById = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        sessionRepository.exclude(session);
        connection.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        connection.close();
    }
}
