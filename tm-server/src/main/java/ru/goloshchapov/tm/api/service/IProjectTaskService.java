package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Project addProject(String userId, String projectName, String projectDescription);

    @NotNull
    Task addTask(String userId, String taskName, String taskDescription);

    void clearProjects(String userId);

    @Nullable
    List<Project> findAllProjects(String userId);

    @Nullable
    List<Task> findAllByProjectId(String userId, String projectId);

    boolean isEmptyProjectById(String userId, String projectId);

    @Nullable
    List<Task> findAllByProjectName(String userId, String projectName);

    boolean isEmptyProjectByName(String userId, String projectName);

    @Nullable
    List<Task> findAllByProjectIndex(String userId, Integer projectIndex);

    boolean isEmptyProjectByIndex(String userId, Integer projectIndex);

    @Nullable
    Task bindToProjectById(String userId, String taskId, String projectId);

    @Nullable
    Task unbindFromProjectById(String userId, String taskId, String projectId);

    @Nullable
    List<Task> removeAllByProjectId(String userId, String projectId);

    @Nullable
    List<Task> removeAllByProjectName(String userId, String projectName);

    @Nullable
    List<Task> removeAllByProjectIndex(String userId, Integer projectIndex);

    boolean removeAllByUserId(String userId);

    @Nullable
    Project removeProjectById(String userId, String projectId);

    @Nullable
    Project removeProjectByName(String userId, String projectName);

    @Nullable
    Project removeProjectByIndex(String userId, Integer projectIndex);

    void createTestData();

}
