package ru.goloshchapov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.repository.TaskRepository;

import java.sql.Connection;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull TaskRepository getRepository(@NotNull Connection connection);

    @NotNull Task add (String userId, String name, String description);

    @Nullable
    @SneakyThrows
    Task update(@Nullable Task task);

    @SneakyThrows
    boolean isAbsentById(@NotNull String userId, @NotNull String taskId);

    @SneakyThrows
    boolean isAbsentByName(@NotNull String userId, @NotNull String taskName);
}
