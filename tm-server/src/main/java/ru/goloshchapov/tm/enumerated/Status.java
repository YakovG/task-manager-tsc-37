package ru.goloshchapov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED ("Not Started"),
    IN_PROGRESS ("In Progress"),
    COMPLETE ("Complete");

    @NotNull private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull public String getDisplayName() {
        return displayName;
    }
    
}
