package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ISessionRepository;
import ru.goloshchapov.tm.constant.FieldConst;
import ru.goloshchapov.tm.constant.TableConst;
import ru.goloshchapov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    private final Connection connection;

    public SessionRepository(Connection connection) {
        super(connection);
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    protected final Session fetch (@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setUserId(row.getString(FieldConst.USER_ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        return session;
    }

    @NotNull
    protected String getTableName() {
        return TableConst.SESSION_TABLE;
    }

    @NotNull
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String query = "INSERT INTO `" + getTableName() +
                "`(`id`,`timestamp`,`userId`,`signature`) VALUES(?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setLong(2, session.getTimestamp());
        statement.setString(3, session.getUserId());
        statement.setString(4, session.getSignature());
        statement.execute();
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session merge(@NotNull final Session session) {
       if (contains(session.getId())) return session;
       return add(session);
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String sessionId) {
        @NotNull final String query = "SELECT COUNT(*) FROM `" + getTableName() + "` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @Override
    @SneakyThrows
    public void exclude(@NotNull final Session session) {
        @NotNull final String query = "DELETE FROM `" + getTableName() + "` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.execute();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findByUserId(final String userId) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `userId` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public List<Session> findAllSession() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }
}
