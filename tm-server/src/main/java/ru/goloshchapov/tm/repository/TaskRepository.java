package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.constant.FieldConst;
import ru.goloshchapov.tm.constant.TableConst;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    private final Connection connection;

    public TaskRepository(Connection connection) {
        super(connection);
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    protected final Task fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        @NotNull final String status = row.getString(FieldConst.STATUS);
        switch (status) {
            case ("In Progress"): task.setStatus(Status.IN_PROGRESS); break;
            case ("Complete"): task.setStatus(Status.COMPLETE); break;
            default: task.setStatus(Status.NOT_STARTED);break;
        }
        task.setCreated(prepare(row.getTimestamp(FieldConst.CREATED)));
        task.setDateStart(prepare(row.getTimestamp(FieldConst.DATE_START)));
        task.setDateFinish(prepare(row.getTimestamp(FieldConst.DATE_FINISH)));
        return task;
    }

    @NotNull
    protected final String getTableName() {
        return TableConst.TASK_TABLE;
    }

    @NotNull
    @SneakyThrows
    public Task add(@NotNull Task task) {
        @NotNull final String query = "INSERT INTO `" + getTableName() +
                "`(`id`,`name`,`description`,`userId`,`projectId`,`status`,`created`,`dateStart`,`dateFinish`) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getProjectId());
        statement.setString(6, task.getStatus().getDisplayName());
        statement.setTimestamp(7, prepare(task.getCreated()));
        statement.setTimestamp(8, prepare(task.getDateStart()));
        statement.setTimestamp(9, prepare(task.getDateFinish()));
        statement.execute();
        return task;
    }

    @NotNull
    @SneakyThrows
    public Task add(@NotNull String userId, @NotNull Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @SneakyThrows
    public Task update(@NotNull Task task) {
        @NotNull final String query = "UPDATE `" + getTableName() +
                "` SET `name` = ?, `description` = ?, `userId` = ?, `projectId` = ?, `status` = ?, " +
                "`created` = ?, `dateStart` = ?, `dateFinish` = ? WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setString(3, task.getUserId());
        statement.setString(4, task.getProjectId());
        statement.setString(5, task.getStatus().getDisplayName());
        statement.setTimestamp(6, prepare(task.getCreated()));
        statement.setTimestamp(7, prepare(task.getDateStart()));
        statement.setTimestamp(8, prepare(task.getDateFinish()));
        statement.setString(9, task.getId());
        statement.execute();
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final String query = "SELECT * FROM `" + getTableName() +
                "` WHERE `userId` = ? AND `projectId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task bindToProjectById(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<Task> task = Optional.ofNullable(findOneById(userId, taskId));
        task.ifPresent(t-> task.get().setProjectId(projectId));
        task.ifPresent(this::update);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task unbindFromProjectById(
            @NotNull final String userId, @NotNull final String taskId
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<Task> task = Optional.ofNullable(findOneById(userId, taskId));
        task.ifPresent(t-> task.get().setProjectId(null));
        task.ifPresent(this::update);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> removeAllByProjectId(
            @NotNull final String userId, @NotNull final String projectId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final Optional<List<Task>> tasks = Optional.ofNullable(findAllByProjectId(userId, projectId));
        tasks.ifPresent(t-> tasks.get().forEach(this::remove));
        return tasks.orElse(null);
    }

}
