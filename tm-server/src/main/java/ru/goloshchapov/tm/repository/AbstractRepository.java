package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private final Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    @NotNull protected final List<E> list = new ArrayList<>();

    protected abstract E fetch (@Nullable final ResultSet row);

    protected abstract String getTableName ();

    public Timestamp prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        @NotNull final Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;
    }

    public Date prepare(@Nullable final Timestamp timestamp) {
        if (timestamp == null) return null;
        @NotNull final Date date = new Date(timestamp.getTime());
        return date;
    }

    public abstract E add(@NotNull final E entity);

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E entity: collection) {
            add(entity);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index) {
        return findAll().get(index);
    }

    @Override
    public boolean isAbsentById(@NotNull final String id) {
        return findOneById(id) == null;
    }

    @Override
    public boolean isAbsentByIndex(@NotNull final Integer index) {
        return findOneByIndex(index) == null;
    }

    @Nullable
    @Override
    public String getIdByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(index);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public int size() {
        return findAll().size();
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `" + getTableName() + "`";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneById(@NotNull final String id) {
        if (isAbsentById(id)) return null;
        final E entity = findOneById(id);
        @NotNull final String query = "DELETE FROM `" + getTableName() + "` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        return entity;
    }

    @Nullable
    @Override
    public E removeOneByIndex(@NotNull final Integer index) {
        if (isAbsentByIndex(index)) return null;
        final E entity = findOneByIndex(index);
        return removeOneById(entity.getId());
    }

    @Override
    public void remove(@NotNull final E entity) {
        removeOneById(entity.getId());
    }
}
