package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.constant.FieldConst;
import ru.goloshchapov.tm.constant.TableConst;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final Connection connection;

    public UserRepository (Connection connection) {
        super(connection);
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    protected final User fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPasswordHash(row.getString(FieldConst.PASSWORDHASH));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setFirstname(row.getString(FieldConst.FIRSTNAME));
        user.setLastname(row.getString(FieldConst.LASTNAME));
        user.setMiddlename(row.getString(FieldConst.MIDDLENAME));
        @NotNull final String role = row.getString(FieldConst.ROLE);
        switch (role) {
            case ("Administrator"): user.setRole(Role.ADMIN); break;
            default: user.setRole(Role.USER); break;
        }
        final int locked = row.getInt(FieldConst.LOCKED);
        user.setLocked(locked == 1);
        return user;
    }

    @NotNull
    protected final String getTableName() {
        return TableConst.USER_TABLE;
    }

    @Override
    @SneakyThrows
    public User add(@NotNull User user) {
        @NotNull final String query = "INSERT INTO `" + getTableName() +
                "`(`id`,`login`,`passwordHash`,`email`,`firstname`,`lastname`,`middlename`,`role`,`locked`) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstname());
        statement.setString(6, user.getLastname());
        statement.setString(7, user.getMiddlename());
        statement.setString(8, user.getRole().getDisplayName());
        if (user.isLocked()) statement.setInt(9, 1);
        else statement.setInt(9, 0);
        statement.execute();
        return user;
    }

    @Nullable
    @Override
    public User findUserById(@NotNull final String id) {
        return findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findUserByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `login` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findUserByEmail(@NotNull final String email) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `email` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Override
    public final boolean isLoginExists(@NotNull final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public final boolean isEmailExists(@NotNull final String email) {
        return findUserByEmail(email) != null;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        final User user = findUserByLogin(login);
        if (user == null) return null;
        return removeOneById(user.getId());
    }

    @Nullable
    @Override
    public User removeUserByEmail(@NotNull final String email) {
        final User user = findUserByEmail(email);
        if (user == null) return null;
        return removeOneById(user.getId());
    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        return removeOneById(user.getId());
    }

    @Override
    @SneakyThrows
    public User update(@NotNull User user) {
        @NotNull final String query = "UPDATE `" + getTableName() +
                "` SET `login` = ?, `passwordHash` = ?, `email` = ?, " +
                "`firstname` = ?, `lastname` = ?, `middlename` = ? , " +
                "`role` = ?, `locked` = ? WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getFirstname());
        statement.setString(5, user.getLastname());
        statement.setString(6, user.getMiddlename());
        statement.setString(7, user.getRole().getDisplayName());
        if (user.isLocked()) statement.setInt(8, 1);
        else statement.setInt(8, 0);
        statement.setString(9, user.getId());
        statement.execute();
        return user;
    }

}
