package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.constant.FieldConst;
import ru.goloshchapov.tm.constant.TableConst;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    private final Connection connection;

    public ProjectRepository(Connection connection) {
        super(connection);
        this.connection = connection;
    }

    @Nullable
    @SneakyThrows
    protected final Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setUserId(row.getString(FieldConst.USER_ID));
        @NotNull final String status = row.getString(FieldConst.STATUS);
        switch (status) {
            case ("In Progress"): project.setStatus(Status.IN_PROGRESS); break;
            case ("Complete"): project.setStatus(Status.COMPLETE); break;
            default: project.setStatus(Status.NOT_STARTED);break;
        }
        project.setCreated(prepare(row.getTimestamp(FieldConst.CREATED)));
        project.setDateStart(prepare(row.getTimestamp(FieldConst.DATE_START)));
        project.setDateFinish(prepare(row.getTimestamp(FieldConst.DATE_FINISH)));
        return project;
    }

    @NotNull
    protected final String getTableName() {
        return TableConst.PROJECT_TABLE;
    }

    @NotNull
    @SneakyThrows
    public Project add(@NotNull Project project) {
        @NotNull final String query = "INSERT INTO `" + getTableName() +
                "`(`id`,`name`,`description`,`userId`,`status`,`created`,`dateStart`,`dateFinish`) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().getDisplayName());
        statement.setTimestamp(6, prepare(project.getCreated()));
        statement.setTimestamp(7, prepare(project.getDateStart()));
        statement.setTimestamp(8, prepare(project.getDateFinish()));
        statement.execute();
        return project;
    }

    @NotNull
    @SneakyThrows
    public Project add(@NotNull String userId, @NotNull Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @SneakyThrows
    public Project update(@NotNull Project project) {
        @NotNull final String query = "UPDATE `" + getTableName() +
                "` SET `name` = ?, `description` = ?, `userId` = ?, `status` = ?, " +
                "`created` = ?, `dateStart` = ?, `dateFinish` = ? WHERE `id` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setString(3, project.getUserId());
        statement.setString(4, project.getStatus().getDisplayName());
        statement.setTimestamp(5, prepare(project.getCreated()));
        statement.setTimestamp(6, prepare(project.getDateStart()));
        statement.setTimestamp(7, prepare(project.getDateFinish()));
        statement.setString(8, project.getId());
        statement.execute();
        return project;
    }
}
