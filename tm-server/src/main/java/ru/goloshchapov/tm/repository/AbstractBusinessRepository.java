package ru.goloshchapov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<M extends AbstractBusinessEntity>
        extends AbstractRepository<M> implements IBusinessRepository<M> {

    private final Connection connection;

    public AbstractBusinessRepository(Connection connection) {
        super(connection);
        this.connection = connection;
    }

    public Predicate<M> predicateByStarted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() != Status.NOT_STARTED);
    }

    public Predicate<M> predicateByCompleted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() == Status.COMPLETE);
    }

    @NotNull
    private M start (@NotNull final M m) {
        m.setStatus(Status.IN_PROGRESS);
        m.setDateStart(new Date());
        update(m);
        return m;
    }

    @NotNull
    private M finish (@NotNull final M m) {
        m.setStatus(Status.COMPLETE);
        m.setDateFinish(new Date());
        update(m);
        return m;
    }

    public abstract M update(@NotNull final M model);

    @Nullable
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model);

    @Override
    public void addAll(@NotNull final String userId, @Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection.forEach(c->c.setUserId(userId));
        for (M model: collection) {
            add(model);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAllByUserId(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public List<M> findAllStarted(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        final List<M> result = findAllByUserId(userId);
        if (result == null) return null;
        return result.stream()
                .filter(predicateByStarted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAllCompleted(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        final List<M> result = findAllByUserId(userId);
        if (result == null) return null;
        return result.stream()
                .filter(predicateByCompleted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String modelId) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() +
                "` WHERE `id` = ? AND `userId` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, modelId);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final String query = "SELECT * FROM `" + getTableName() + "`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<M> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        @NotNull final M model = result.get(index);
        if (model.checkUserAccess(userId)) return model;
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByName(@NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() +
                "` WHERE `name` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `" + getTableName() +
                "` WHERE `name` = ? AND `userId` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Override
    public boolean isAbsentById(@NotNull final String userId, @NotNull final String modelId) {
        final M model = findOneById(userId, modelId);
        if (model == null) return true;
        return (!model.checkUserAccess(userId));
    }

    @Override
    public boolean isAbsentByName(@NotNull final String name) {
        return findOneByName(name) == null;
    }

    @Override
    public boolean isAbsentByName(@NotNull final String userId, @NotNull final String name) {
        final M model = findOneByName(userId, name);
        if (model == null) return true;
        return (!model.checkUserAccess(userId));
    }

    @Nullable
    @Override
    public String getIdByName(final String name) {
        @Nullable final M model = findOneByName(name);
        if (model == null) return null;
        return model.getId();
    }

    @Override
    public int size(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return 0;
        return models.size();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@NotNull final String userId, @NotNull final String modelId) {
        if (isAbsentById(userId, modelId)) return null;
        final M model = findOneById(userId, modelId);
        if (model == null) return null;
        @NotNull final String query = "DELETE FROM `" + getTableName() +
                "` WHERE `id` = ? AND `userId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, modelId);
        statement.setString(2, userId);
        statement.execute();
        return model;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        removeOneById(userId, model.getId());
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM `" + getTableName() + "` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOneById(model.getId());
    }

    @Nullable
    @Override
    public M removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final M model = findOneByName(userId, name);
        if (model == null) return null;
        return removeOneById(model.getId());
    }

    @Nullable
    @Override
    public M startOneById(@NotNull final String userId, @NotNull final String modelId) {
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        @NotNull final M modelStarted = start(model);
        return update(modelStarted);
    }

    @Nullable
    @Override
    public M startOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final M modelStarted = start(model);
        return update(modelStarted);
    }

    @Nullable
    @Override
    public M startOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        @NotNull final M modelStarted = start(model);
        return update(modelStarted);
    }

    @Nullable
    @Override
    public M finishOneById(@NotNull final String userId, @NotNull final String modelId) {
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        @NotNull final M modelFinished = finish(model);
        return update(modelFinished);

    }

    @Nullable
    @Override
    public M finishOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final M modelFinished = finish(model);
        return update(modelFinished);
    }

    @Nullable
    @Override
    public M finishOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        @NotNull final M modelFinished = finish(model);
        return update(modelFinished);
    }

}
