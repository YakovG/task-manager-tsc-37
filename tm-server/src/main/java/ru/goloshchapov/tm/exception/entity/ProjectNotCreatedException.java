package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class ProjectNotCreatedException extends AbstractException {

    public ProjectNotCreatedException() {
        super("Error! Project not created...");
    }

}
