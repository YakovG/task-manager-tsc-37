package ru.goloshchapov.tm.exception.empty;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    public EmptyPasswordHashException() { super("Error! Password hash is empty..."); }

}
