package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;
import ru.goloshchapov.tm.api.service.ITestService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class TestService implements ITestService {

    private final IPropertyService propertyService;

    public TestService(IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = propertyService.getJdbcTestUsername();
        @NotNull final String password = propertyService.getJdbcTestPassword();
        @NotNull final String url = propertyService.getJdbcTestUrl();
        final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }


    @Override
    @SneakyThrows
    public void initTestUserTable() {
        final Connection connection = getConnection();
        try {
            connection.createStatement().executeUpdate(
                    "CREATE TABLE `tm_user` ( " +
                            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
                            "`login` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`passwordHash` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`firstname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`lastname` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`middlename` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`role` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`locked` BIT(1) NULL DEFAULT NULL, " +
                            "PRIMARY KEY (`id`) USING BTREE " +
                            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;"
            );
            connection.commit();
        } catch (final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestSessionTable() {
        final Connection connection = getConnection();
        try {
            connection.createStatement().executeUpdate(
                    "CREATE TABLE `tm_session` ( " +
                            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
                            "`timestamp` BIGINT(20) NOT NULL DEFAULT '0', " +
                            "`userId` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`signature` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "PRIMARY KEY (`id`) USING BTREE, " +
                            "INDEX `FK_tm_session_tm_user` (`userId`) USING BTREE " +
                            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;"
            );
            connection.commit();
        }
        catch (final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestProjectTable() {
        final Connection connection = getConnection();
        try {
            connection.createStatement().executeUpdate(
                    "CREATE TABLE `tm_project` ( " +
                            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
                            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`created` TIMESTAMP NULL DEFAULT NULL, " +
                            "`dateStart` TIMESTAMP NULL DEFAULT NULL, " +
                            "`dateFinish` TIMESTAMP NULL DEFAULT NULL, " +
                            "PRIMARY KEY (`id`) USING BTREE, " +
                            "INDEX `FK_tm_project_tm_user` (`userId`) USING BTREE " +
                            ") COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;"
            );
            connection.commit();
        }
        catch (final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void initTestTaskTable() {
        final Connection connection = getConnection();
        try {
            connection.createStatement().executeUpdate(
                    "CREATE TABLE `tm_task` ( " +
                            "`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci', " +
                            "`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`projectId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci', " +
                            "`created` TIMESTAMP NULL DEFAULT NULL, " +
                            "`dateStart` TIMESTAMP NULL DEFAULT NULL, " +
                            "`dateFinish` TIMESTAMP NULL DEFAULT NULL, " +
                            "PRIMARY KEY (`id`) USING BTREE, " +
                            "INDEX `FK_tm_task_tm_user` (`userId`) USING BTREE, " +
                            "INDEX `FK_tm_task_tm_project` (`projectId`) USING BTREE " +
                            ") COLLATE='utf8_unicode_ci' ENGINE=InnoDB;"

            );
            connection.commit();
        }
        catch (final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void dropDatabase() {
        final Connection connection = getConnection();
        connection.createStatement().executeUpdate(
                "DROP DATABASE IF EXISTS testtm;"
        );
        connection.close();
    }
}
