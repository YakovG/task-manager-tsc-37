package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class ElementsNotFoundException extends AbstractException {

    public ElementsNotFoundException() {
        super("Error! No elements found - list is empty...");
    }

}
